import { Activity } from './activity';
import {Person} from './person';

export class Child extends Person {
  id: string;
  section: string;
  activites: Activity[];
}
