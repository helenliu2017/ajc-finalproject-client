export class Activity {
  id: string;
  name: string;
  sectionAllowed: string;
  maxNumChildren: number;
}
