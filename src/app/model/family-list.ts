export class FamilyList {
  id: string;
  fatherName: string;
  motherName: string;
  childrenNumber: number;
  address: string;
}
