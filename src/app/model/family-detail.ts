import {Address} from './address';
import {Child} from './child';
import {Parent} from './parent';
export class FamilyDetail {
  id: string;
  father: Parent;
  mother: Parent;
  children: Child[];
  address: Address;
  constructor() {
    this.father = new Parent();
    this.mother = new Parent();
    this.address = new Address();
  }
}
