export class Address {
    number: number;
    road: string;
    postalCode: number;
    city: string;
}
