import { FamilyList } from '../model/family-list';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminFamilyService } from '../service/adminFamilyService';
import { FamilyDetail } from '../model/family-detail';

@Component({
  selector: 'app-family-list',
  templateUrl: './family-list.component.html',
  styleUrls: ['./family-list.component.css']
})

export class FamilyListComponent implements OnInit {
  families: Array<FamilyList>;

  constructor(private route: Router,
    private adminFamilyService: AdminFamilyService) {}

  getAllFamilies() {
    this.adminFamilyService.getFamilies().subscribe
      (data => this.families = data, error => console.log(error));
    return false;
  }

  showFamilyDetail() {
    console.log(JSON.stringify(FamilyDetail));
    this.router.navigate(['/family-update', familyDetail.id]);
  }
deleteFamily(familyList) {
    console.log('Famille à supprimer : ' + JSON.stringify(familyList));

    const dialog = 'Veuillez supprimer la famille M.' + familyList.fatherName + ' et Mme.' + familyList.motherName
        + ' et toutes informations associés?';
    if (confirm(dialog)) {
      this.adminFamilyService.deleteFamily(familyList.id)
        .subscribe(() => { this.getAllFamilies(); },
          error => console.log(error));
    }
  }
  ngOnInit() {
    console.log('family-list component works');
    this.getAllFamilies();
  }

}
