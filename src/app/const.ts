/**
 * Define a constant of localhost url
 */

export const REST_HOST = 'http://localhost:8080/api';
