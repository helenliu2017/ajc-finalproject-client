import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FamilyListComponent } from './family-list/family-list.component';
import { FamilyUpdateComponent } from './family-update/family-update.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { RoutingComponent } from './routing/routing.component';
import { AdminFamilyService } from './serivce/admin-family.service';


export const ROUTES: Routes = [
  { path: '', component: AppComponent},
  { path: 'families', component: FamilyListComponent},
  { path: 'family/:id', component: FamilyUpdateComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    FamilyListComponent,
    FamilyUpdateComponent,
    InscriptionComponent,
    RoutingComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    RoutingComponent
  ],
  providers: [AdminFamilyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
