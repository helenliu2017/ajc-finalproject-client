import { TestBed, inject } from '@angular/core/testing';

import { AdminChildService } from './admin-child.service';

describe('AdminChildService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminChildService]
    });
  });

  it('should be created', inject([AdminChildService], (service: AdminChildService) => {
    expect(service).toBeTruthy();
  }));
});
