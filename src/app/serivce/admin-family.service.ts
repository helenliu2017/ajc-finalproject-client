import { FamilyDetail } from '../model/family-detail';
import { FamilyList } from '../model/family-list';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as CONST from '../const';

@Injectable()
export class AdminFamilyService {

  private family_url = CONST.REST_HOST + '/families';

  constructor(private http: HttpClient) { }

  getFamilies(): Observable<FamilyList[]> {
  return this.http.get<FamilyList[]>(this.family_url);
  }

  getFamilyDetail(id): Observable<FamilyDetail> {
  return this.http.get<FamilyDetail>(this.family_url + '/' + id);
  }

  addFamily(family): Observable<FamilyDetail> {
  return this.http.post<FamilyDetail>(this.family_url, family);
  }

  updateFamily(family): Observable<FamilyDetail> {
  return this.http.put<FamilyDetail>(this.family_url, family);
  }

  deleteFamily(id): Observable<any> {
  return this.http.delete(this.family_url + '/' + id);
  }

  addChildToFamily(id, newChild): Observable<FamilyDetail> {
  return this.http.post<FamilyDetail>(this.family_url + '/' + id + '/children', newChild());
  }

}
