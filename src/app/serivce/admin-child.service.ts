import { Activity } from '../model/activity';
import { Child } from '../model/child';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import * as CONST from '../const';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AdminChildService {

  private child_url = CONST.REST_HOST + '/children';

  constructor(private http: HttpClient) { }

  getChildDetail(id): Observable<Child> {
  return this.http.get<Child>(this.child_url + '/' + id);
  }

  getActivitesToInscribe(id): Observable<Activity[]> {
  return this.http.get<Activity[]>(this.child_url + '/' + id + '/activites');
  }

}
