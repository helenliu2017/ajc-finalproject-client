import { TestBed, inject } from '@angular/core/testing';

import { AdminFamilyService } from './admin-family.service';

describe('AdminFamilyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminFamilyService]
    });
  });

  it('should be created', inject([AdminFamilyService], (service: AdminFamilyService) => {
    expect(service).toBeTruthy();
  }));
});
